from msilib.schema import ListBox
import tkinter as tk
import fnmatch
import os
from Pygame import mixer

canvas = tk.Tk()
canvas.title("Player")
canvas.geometry("600x800")
canvas.config(bg = 'black')

rootpath = "C:\\Users\Тахир\Desktop\zz"
pattern = "*.mp3"
mixer.init()

prev_img = tk.PhotoImage(file = "prev_img.png")
stop_img = tk.PhotoImage(file = "stop_img.png")
play_img = tk.PhotoImage(file = "play_img.png")
pause_img = tk.PhotoImage(file = "pause_img.png")
next_img = tk.PhotoImage(file = "next_img.png")

def select():
    label.config(taxt = listBox.get("anchor"))
    mixer.music.load(rootpath + "\\" + listBox.get("anchor"))
    mixer.music.play()

def stop():
    mixer.music.stop()
    listBox.select_clear('active')

def play_next():
    next_sond = listBox.curselection()
    next_sond = next_sond[0]+1
    next_sond_name = listBox.get(next_sond)

    mixer.music.load(rootpath + "\\" + listBox.get("anchor"))
    mixer.music.play()

    listBox.select_clear(0, 'end')
    listBox.activate(next_sond)
    listBox.select_set(next_sond)

def play_prev():
    next_sond = listBox.curselection()
    next_sond = next_sond[0]-1
    next_sond_name = listBox.get(next_sond)

    mixer.music.load(rootpath + "\\" + listBox.get("anchor"))
    mixer.music.play()

    listBox.select_clear(0, 'end')
    listBox.activate(next_sond)
    listBox.select_set(next_sond)

def pause_sond():
    if pauseButtom["text"] == "Pause":
        mixer.music.pause()
        pauseButtom["text"] = "Play"
    else:
        mixer.music.unpause()
        pauseButtom["text"] = "Pause"


listBox = tk.Listbox(canvas, fg="cyan", bg = "black", width = 100, font = ('poppins', 14))
listBox.pack(padx = 15, pady = 15 )

label = tk.Label(canvas, text = '', bg = 'black', fg = 'yellow', font = ('poppins', 14))
label.pack(pady = 15)

top=tk.Frame(canvas, bg = "black")
top.pack(padx=10, pady=5, anchor='center')

prevButtom = tk.Button(canvas, text = "Prev", image = prev_img, bg = 'black', borderwidth = 0, command=play_prev )
prevButtom.pack(pady = 15, in_ = top, side = 'left')

stopButtom = tk.Button(canvas, text = "Stop",  image = stop_img, bg = 'black', borderwidth = 0, command = stop)
stopButtom.pack(pady = 15, in_ = top, side = 'left')

playButtom = tk.Button(canvas, text = "Play",  image = play_img, bg = 'black', borderwidth = 0, command=select  )
playButtom.pack(pady = 15, in_ = top, side = 'left')

pauseButtom = tk.Button(canvas, text = "Pause",  image = pause_img, bg = 'black', borderwidth = 0, cammand = pause_sond )
pauseButtom.pack(pady = 15, in_ = top, side = 'left')

nextButtom = tk.Button(canvas, text = "Next",  image = stop_img, bg = 'black', borderwidth = 0, command=play_next )
nextButtom.pack(pady = 15, in_ = top, side = 'left')


for root,dirs,files in os.walk(rootpath):
    for filename in fnmatch.filter(files, pattern):
        listBox.insert('end', filename)

canvas.mainloop()